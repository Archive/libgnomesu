# libgnomesu.HEAD.po faylının Azərbaycan Dilinə tərcüməsi
# libgnomesu.HEAD.po faylının Azərbaycan dilinə tərcüməsi
# This file is distributed under the same license as the PACKAGE package.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# Mətin Əmirov <metin@karegen.com>, 2003
#
msgid ""
msgstr ""
"Project-Id-Version: libgnomesu.HEAD\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-01-08 18:51+0100\n"
"PO-Revision-Date: 2003-10-09 19:41+0300\n"
"Last-Translator: Mətin Əmirov <metin@karegen.com>\n"
"Language-Team: Azərbaycan Dili <gnome@azətt.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.0.1\n"

#: src/gnomesu-auth-dialog.c:110
msgid "Password needed"
msgstr "Şifrə lazımdır"

#: src/gnomesu-auth-dialog.c:163
#, fuzzy
msgid "Command:"
msgstr "İcra ediləcək əmr:"

#: src/gnomesu-auth-dialog.c:213
msgid "C_ontinue"
msgstr ""

#: src/gnomesu-auth-dialog.c:363
msgid "Administrator (root) privilege is required."
msgstr ""

#: src/gnomesu-auth-dialog.c:364
#, fuzzy
msgid "Please enter the root password to continue."
msgstr ""
"Xahiş edirik, %s şifrəsini bildirin və davam etmək üçün İcra Et düyməsinə "
"basın."

#: src/gnomesu-auth-dialog.c:413
#, fuzzy
msgid "_Password:"
msgstr "Ş_ifrəniz:"

#: src/gnomesu-auth-dialog.c:438
msgid "Please wait, verifying password..."
msgstr "Xahiş edirik gözləyin, şifrə doğrulanır..."

#: src/gnomesu-auth-dialog.c:446
msgid "Incorrect password, please try again."
msgstr "Hökmsüz şifrə, xahiş edirik yenə sınayın."

#: src/gnomesu-auth-dialog.c:453
msgid "Incorrect password, please try again. You have one more chance."
msgstr "Hökmsüz şifrə, xahiş edirik yenə sınayın. Bir imkanınız daha var."

#: src/libgnomesu.c:168
msgid "No services for libgnomesu are available.\n"
msgstr ""

#: src/services/pam.c:193 src/services/su.c:186
#, fuzzy, c-format
msgid "Please enter %s's password and click Continue."
msgstr ""
"Xahiş edirik, %s şifrəsini bildirin və davam etmək üçün İcra Et düyməsinə "
"basın."

#: src/services/pam.c:195 src/services/su.c:188
msgid "The requested action needs further authentication."
msgstr "İstənən gedişat daha irəli dərəcədə səlahiyyətləndirilməlidir."

#: src/services/pam.c:201 src/services/su.c:194
#, c-format
msgid "%s's _password:"
msgstr "%s istifadəçisinin ş_ifrəsi:"

#: src/services/pam.c:220 src/services/su.c:213
#, c-format
msgid "User '%s' doesn't exist."
msgstr "'%s' istifadəçisi mövcud deyil."

#: src/services/pam.c:225 src/services/su.c:217
msgid "An unknown error occured while authenticating."
msgstr "İcazə verilirkən xəta yarandı."

#: src/services/pam.c:229 src/services/su.c:221
msgid "You do not have permission to authenticate."
msgstr "İcazə üçün səlahiyyətləriniz yoxdur."

#: src/services/pam.c:233
msgid "Unable to initialize the PAM authentication system."
msgstr ""

#: su-backend/closeout.c:71 su-backend/closeout.c:73
msgid "write error"
msgstr "yazma xətası"

#: su-backend/version-etc.c:38
#, c-format
msgid "Written by %s.\n"
msgstr "%s tərəfindən yazılıb.\n"

#: su-backend/version-etc.c:39
msgid ""
"\n"
"Copyright (C) 1999 Free Software Foundation, Inc.\n"
"This is free software; see the source for copying conditions.  There is NO\n"
"warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n"
msgstr ""
"\n"
"Copyright (C) 1999 Free Software Foundation, Inc.\n"
"This is free software; see the source for copying conditions.  There is NO\n"
"warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n"

#: tools/gnomesu.c:40
msgid "Pass the command to execute as one single string."
msgstr "İcra ediləcək əmri tək qatar formasında ötürün"

#: tools/gnomesu.c:41
msgid "COMMAND"
msgstr "ƏMR"

#: tools/gnomesu.c:44
msgid "Run as this user instead of as root."
msgstr "Ali istifadəçi yerinə bu istifadəçi altında icra et."

#: tools/gnomesu.c:45
msgid "USERNAME"
msgstr "İSTAFADƏÇİADI"

#: tools/gnomesu.c:65
msgid "GNOME SuperUser"
msgstr "GNOME SuperUser"

#~ msgid "GNOME SuperUser integration"
#~ msgstr "GNOME Ali İstifadəçi inteqrasiyası"

#~ msgid "Open as superuser (root)..."
#~ msgstr "Ali istifadəçi (root) olaraq aç..."

#~ msgid ""
#~ "You do not have permission gain superuser (root) privileges. This "
#~ "incident will be reported."
#~ msgstr ""
#~ "Ali istifadəçi icazəsinə malik olma səlahiyyətiniz yoxdur. Bu hadisə "
#~ "raport ediləcək."

#~ msgid "_Run"
#~ msgstr "_İcra Et"

#~ msgid ""
#~ "<span weight=\"bold\">The requested action needs superuser (root) "
#~ "privileges.</span>\n"
#~ "Please enter the superuser password and click Run to continue."
#~ msgstr ""
#~ "<span weight=\"bold\">İstənən gedişat üçün ali istifadəçi şifrəsi "
#~ "lazımdır.</span>\n"
#~ "Xahiş edirik, ali istifadəçi şifərsini bildirin və davam etmək üçün İcra "
#~ "Et deyməsinə basın."

#~ msgid "Superuser _password:"
#~ msgstr "Ali istifadəçi ş_ifrəsi:"

#~ msgid "*"
#~ msgstr "*"

#~ msgid ""
#~ "<span style=\"italic\" weight=\"bold\">Please wait, verifying password..."
#~ "</span>"
#~ msgstr ""
#~ "<span style=\"italic\" weight=\"bold\">Xahiş edirik gözləyin, şifrə "
#~ "doğrulanır...</span>"

#~ msgid "User's password needed"
#~ msgstr "İstifadəçinin şifrəsi lazımdır"

#~ msgid ""
#~ "<span weight=\"bold\">The requested action needs autentication.</span>\n"
#~ "Please enter your password and click Run to continue."
#~ msgstr ""
#~ "<span weight=\"bold\">İstənən gedişat üçün icazə lazımdır.</span>\n"
#~ "Xahiş edirik, şifrənizi bildirin və davam etmək üçün İcra Et düyməsinə "
#~ "basın."
