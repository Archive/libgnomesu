# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: libgnomesu \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-01-08 18:51+0100\n"
"PO-Revision-Date: 2004-11-25 00:26+0900\n"
"Last-Translator: Young-Ho Cha <ganadist at mizi.com>\n"
"Language-Team: GNOME Korea <gnome-kr-hackers@lists.kldp.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/gnomesu-auth-dialog.c:110
msgid "Password needed"
msgstr "열쇠글이 필요합니다"

#: src/gnomesu-auth-dialog.c:163
#, fuzzy
msgid "Command:"
msgstr "실행할 명령:"

#: src/gnomesu-auth-dialog.c:213
msgid "C_ontinue"
msgstr ""

#: src/gnomesu-auth-dialog.c:363
msgid "Administrator (root) privilege is required."
msgstr ""

#: src/gnomesu-auth-dialog.c:364
#, fuzzy
msgid "Please enter the root password to continue."
msgstr "%s의 열쇠글을 넣고 실행을 누르십시오."

#: src/gnomesu-auth-dialog.c:413
#, fuzzy
msgid "_Password:"
msgstr "열쇠글(_P):"

#: src/gnomesu-auth-dialog.c:438
msgid "Please wait, verifying password..."
msgstr "잠시만 기다리십시오. 열쇠글을 확인하고 있습니다..."

#: src/gnomesu-auth-dialog.c:446
msgid "Incorrect password, please try again."
msgstr "열쇠글이 틀렸습니다, 다시 입력하십시오."

#: src/gnomesu-auth-dialog.c:453
msgid "Incorrect password, please try again. You have one more chance."
msgstr "열쇠글이 틀렸습니다. 다시 입력하십시오."

#: src/libgnomesu.c:168
msgid "No services for libgnomesu are available.\n"
msgstr "libgnomesu의 서비스를 쓸 수 없습니다.\n"

#: src/services/pam.c:193 src/services/su.c:186
#, fuzzy, c-format
msgid "Please enter %s's password and click Continue."
msgstr "%s의 열쇠글을 넣고 실행을 누르십시오."

#: src/services/pam.c:195 src/services/su.c:188
msgid "The requested action needs further authentication."
msgstr "요청한 작업은 인증을 필요합니다."

#: src/services/pam.c:201 src/services/su.c:194
#, c-format
msgid "%s's _password:"
msgstr "%s의 열쇠글(_P):"

#: src/services/pam.c:220 src/services/su.c:213
#, c-format
msgid "User '%s' doesn't exist."
msgstr "'%s'사용자가 없습니다."

#: src/services/pam.c:225 src/services/su.c:217
msgid "An unknown error occured while authenticating."
msgstr "인증 중 알 수 없는 오류가 생겼습니다."

#: src/services/pam.c:229 src/services/su.c:221
msgid "You do not have permission to authenticate."
msgstr "인증할 허가권이 없습니다."

#: src/services/pam.c:233
#, fuzzy
msgid "Unable to initialize the PAM authentication system."
msgstr "PAM 인증 체계를 초기화할 수 없습니다."

#: su-backend/closeout.c:71 su-backend/closeout.c:73
msgid "write error"
msgstr "쓰기 오류"

#: su-backend/version-etc.c:38
#, c-format
msgid "Written by %s.\n"
msgstr "만든이: %s.\n"

#: su-backend/version-etc.c:39
msgid ""
"\n"
"Copyright (C) 1999 Free Software Foundation, Inc.\n"
"This is free software; see the source for copying conditions.  There is NO\n"
"warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n"
msgstr ""
"\n"
"Copyright (C) 1999 Free Software Foundation, Inc.\n"
"This is free software; see the source for copying conditions.  There is NO\n"
"warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n"

#: tools/gnomesu.c:40
msgid "Pass the command to execute as one single string."
msgstr "하나의 문자열로 실행할 명령을 넘깁니다."

#: tools/gnomesu.c:41
msgid "COMMAND"
msgstr "명령"

#: tools/gnomesu.c:44
msgid "Run as this user instead of as root."
msgstr "root대신 이 사용자로 실행합니다."

#: tools/gnomesu.c:45
msgid "USERNAME"
msgstr "사용자이름"

#: tools/gnomesu.c:65
msgid "GNOME SuperUser"
msgstr "그놈 슈퍼유저"

#~ msgid "GNOME SuperUser integration"
#~ msgstr "그놈 슈퍼유저 통합"

#~ msgid "Open as superuser (root)..."
#~ msgstr "관리자 권한으로 열기..."

#~ msgid ""
#~ "You do not have permission gain superuser (root) privileges. This "
#~ "incident will be reported."
#~ msgstr ""
#~ "관리자 권한을 얻을 수 있는 허가권이 없습니다. 이 사건은 보고될것 입니다."

#~ msgid "_Run"
#~ msgstr "실행(_R)"

#~ msgid ""
#~ "<span weight=\"bold\">The requested action needs superuser (root) "
#~ "privileges.</span>\n"
#~ "Please enter the superuser password and click Run to continue."
#~ msgstr ""
#~ "<span weight=\"bold\">요청한 작업은 관리자 권한이 필요합니다.</span>\n"
#~ "관리자 열쇠글을 입력하고 실행을 누르십시오."

#~ msgid "Superuser _password:"
#~ msgstr "관리자 열쇠글(_P):"

# FIXME 이게 더 이쁘지 않을까요?
#~ msgid "*"
#~ msgstr "●"

#~ msgid ""
#~ "<span style=\"italic\" weight=\"bold\">Please wait, verifying password..."
#~ "</span>"
#~ msgstr ""
#~ "<span style=\"italic\" weight=\"bold\">잠시만 기다리십시오.열쇠글을 확인하"
#~ "고 있습니다...</span>"

#~ msgid "User's password needed"
#~ msgstr "사용자의 열쇠글이 필요합니다"

#~ msgid ""
#~ "<span weight=\"bold\">The requested action needs autentication.</span>\n"
#~ "Please enter your password and click Run to continue."
#~ msgstr ""
#~ "<span weight=\"bold\">요청한 작업은 인증을 필요합니다.</span>\n"
#~ "열쇠글을 입력하고 실행을 누르십시오."
