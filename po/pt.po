# libgnomesu's Portuguese Translation.
# Copyright © 2003, 2004 libgnomesu
# This file is distributed under the same license as the libgnomesu 
# Duarte Loreto <happyguy_pt@hotmail.com>, 2003, 2004.
#
msgid ""
msgstr ""
"Project-Id-Version: 2.8\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-01-08 18:51+0100\n"
"PO-Revision-Date: 2004-12-09 22:40+0000\n"
"Last-Translator: Duarte Loreto <happyguy_pt@hotmail.com>\n"
"Language-Team: Portuguese <gnome_pt@yahoogroups.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/gnomesu-auth-dialog.c:110
msgid "Password needed"
msgstr "É necessária uma senha"

#: src/gnomesu-auth-dialog.c:163
msgid "Command:"
msgstr "Comando:"

#: src/gnomesu-auth-dialog.c:213
msgid "C_ontinue"
msgstr ""

#: src/gnomesu-auth-dialog.c:363
#, fuzzy
msgid "Administrator (root) privilege is required."
msgstr ""
"<b>São necessários privilégios de super-utilizador (root).</b>\n"
"Introduza a senha de root para continuar."

#: src/gnomesu-auth-dialog.c:364
#, fuzzy
msgid "Please enter the root password to continue."
msgstr "Introduza a senha de %s e prima Executar para continuar."

#: src/gnomesu-auth-dialog.c:413
msgid "_Password:"
msgstr "_Senha:"

#: src/gnomesu-auth-dialog.c:438
msgid "Please wait, verifying password..."
msgstr "Aguarde, a verificar a senha..."

#: src/gnomesu-auth-dialog.c:446
msgid "Incorrect password, please try again."
msgstr "Senha incorrecta, tente novamente."

#: src/gnomesu-auth-dialog.c:453
msgid "Incorrect password, please try again. You have one more chance."
msgstr "Senha incorrecta, tente novamente. Tem mais uma hipótese."

#: src/libgnomesu.c:168
msgid "No services for libgnomesu are available.\n"
msgstr "Não estão disponíveis serviços para a libgnomesu.\n"

#: src/services/pam.c:193 src/services/su.c:186
#, fuzzy, c-format
msgid "Please enter %s's password and click Continue."
msgstr "Introduza a senha de %s e prima Executar para continuar."

#: src/services/pam.c:195 src/services/su.c:188
msgid "The requested action needs further authentication."
msgstr "A acção pedida necessita de autenticação adicional."

#: src/services/pam.c:201 src/services/su.c:194
#, c-format
msgid "%s's _password:"
msgstr "A _senha de %s:"

#: src/services/pam.c:220 src/services/su.c:213
#, c-format
msgid "User '%s' doesn't exist."
msgstr "O utilizador '%s' não existe."

#: src/services/pam.c:225 src/services/su.c:217
msgid "An unknown error occured while authenticating."
msgstr "Ocorreu um erro desconhecido ao autenticar-se."

#: src/services/pam.c:229 src/services/su.c:221
msgid "You do not have permission to authenticate."
msgstr "Não possui permissões para se autenticar."

#: src/services/pam.c:233
msgid "Unable to initialize the PAM authentication system."
msgstr "Incapaz de inicializar o sistema de autenticação PAM."

#: su-backend/closeout.c:71 su-backend/closeout.c:73
msgid "write error"
msgstr "erro de escrita"

#: su-backend/version-etc.c:38
#, c-format
msgid "Written by %s.\n"
msgstr "Escrito por %s.\n"

#: su-backend/version-etc.c:39
msgid ""
"\n"
"Copyright (C) 1999 Free Software Foundation, Inc.\n"
"This is free software; see the source for copying conditions.  There is NO\n"
"warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n"
msgstr ""
"\n"
"Copyright © 1999 Free Software Foundation, Inc.\n"
"Esta é uma aplicação livre; consulte no código as condições de cópia.  NÃO\n"
"existe qualquer garantia; nem mesmo a de COMERCIABILIDADE ou ADEQUAÇÃO A UM "
"FIM ESPECÍFICO.\n"

#: tools/gnomesu.c:40
msgid "Pass the command to execute as one single string."
msgstr "Passar o comando a executar como uma única expressão."

#: tools/gnomesu.c:41
msgid "COMMAND"
msgstr "COMANDO"

#: tools/gnomesu.c:44
msgid "Run as this user instead of as root."
msgstr "Executar com este utilizador em vez de root."

#: tools/gnomesu.c:45
msgid "USERNAME"
msgstr "UTILIZADOR"

#: tools/gnomesu.c:65
msgid "GNOME SuperUser"
msgstr "Super-Utilizador GNOME"

#~ msgid "GNOME SuperUser integration"
#~ msgstr "Integração de Super-Utilizador no GNOME"

#~ msgid "Open as superuser (root)..."
#~ msgstr "Abrir como super-utilizador (root)..."

#~ msgid ""
#~ "You do not have permission gain superuser (root) privileges. This "
#~ "incident will be reported."
#~ msgstr ""
#~ "Não possui permissões para obter privilégios de super-utilizador (root). "
#~ "Este incidente será reportado."

#~ msgid "User's password needed"
#~ msgstr "É necessária a senha do utilizador"

#~ msgid "_Run"
#~ msgstr "E_xecutar"

#~ msgid ""
#~ "<span weight=\"bold\">The requested action needs autentication.</span>\n"
#~ "Please enter your password and click Run to continue."
#~ msgstr ""
#~ "<span weight=\"bold\">A acção requerida necessita de autenticação.</"
#~ "span>\n"
#~ "Introduza a sua senha e prima Executar para continuar."

#~ msgid "Your _password:"
#~ msgstr "A sua _senha:"

#~ msgid "*"
#~ msgstr "*"

#~ msgid "Command to run:"
#~ msgstr "Comando a executar:"

#~ msgid ""
#~ "<span style=\"italic\" weight=\"bold\">Please wait, verifying password..."
#~ "</span>"
#~ msgstr ""
#~ "<span style=\"italic\" weight=\"bold\">Aguarde, a verificar a senha...</"
#~ "span>"
